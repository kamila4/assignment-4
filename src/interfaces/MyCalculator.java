package interfaces;

public class MyCalculator implements AdvancedArithmetic{

	@Override
	public int devisor_sum(int n) {
		int sum = 0;
		
		for(int i=1;i<=n;i++) {
			if(i%n==0) sum+=i;
		}
		return sum;
	}

}
